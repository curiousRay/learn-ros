#include "ros/ros.h"
#include "srv_cli/add_ints.h"

bool add (srv_cli::add_ints::Request &req, srv_cli::add_ints::Response &res) {
    res.sum = req.a + req.b;
    ROS_INFO("req: x=%ld, y=%ld", (long int)req.a, (long int)req.b);
    ROS_INFO("sending back response:[%ld]", (long int)res.sum);
    return true;
}

int main (int argc, char **argv) {
    ros::init(argc, argv, "server_node");
    ros::NodeHandle nh;
    ros::ServiceServer service = nh.advertiseService("svc_name", add);
    ROS_INFO("Ready to add two ints.");
    ros::spin();

    return 0;
}