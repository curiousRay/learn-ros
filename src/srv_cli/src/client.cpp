#include "ros/ros.h"
#include "srv_cli/add_ints.h"
#include <cstdlib>

int main(int argc, char **argv) {
    ros::init(argc, argv, "client_node");
    if (argc != 3) {
        ROS_INFO("usage: srv-cli X Y");
        return 1;
    }

    ros::NodeHandle nh;
    ros::ServiceClient client = nh.serviceClient<srv_cli::add_ints>("svc_name");

    srv_cli::add_ints srv;
    srv.request.a = atoll(argv[1]);
    srv.request.b = atoll(argv[2]);

    if(client.call(srv)) {
        ROS_INFO("Sum: %ld", (long int)srv.response.sum);
    } else {
        ROS_ERROR("Failed to call service");
        return 1;
    }

    return 0;
}