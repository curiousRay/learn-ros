#include "ros/ros.h"
#include "std_msgs/String.h"
#include <sstream>

int main(int argc, char **argv) {
  ros::init(argc, argv, "talker");
  //set node name
  
  ros::NodeHandle nh;
  std::string mSentence = "lorem ipsum";
  //if no param is passed in, use this as default
 
  ros::Publisher pub = nh.advertise<std_msgs::String>("mTopic", 1000);
  //set this node as publisher

  ros::Rate loop_rate(10);
  //set publish rate as 10Hz
  
  //if (nh.getParam("sentence", mSentence)) {}
  ros::param::get("sentence", mSentence);

  while (ros::ok()) {
    std_msgs::String msg;
    std::stringstream ss;
    ss << mSentence;
    msg.data = ss.str();
    ROS_INFO("%s", msg.data.c_str());

    pub.publish(msg);

    ros::spinOnce();
    //if a subscriber comes, ros will update and read all topics

    loop_rate.sleep();
    //hang on the program
  }

  return 0;
}
