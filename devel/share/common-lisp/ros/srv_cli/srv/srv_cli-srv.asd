
(cl:in-package :asdf)

(defsystem "srv_cli-srv"
  :depends-on (:roslisp-msg-protocol :roslisp-utils )
  :components ((:file "_package")
    (:file "add_ints" :depends-on ("_package_add_ints"))
    (:file "_package_add_ints" :depends-on ("_package"))
  ))